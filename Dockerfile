FROM telegraf:latest

MAINTAINER Artur Krzywanski <artkrz@gmail.com>

RUN echo "deb http://ftp.pl.debian.org/debian/ stretch main contrib non-free" >> /etc/apt/sources.list
RUN apt-get update
RUN apt-get install --yes snmp-mibs-downloader

RUN download-mibs
RUN sed -i "s/^\(mibs *:\).*/#\1/" /etc/snmp/snmp.conf

COPY NAS.mib /usr/share/snmp/mibs
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["telegraf"]
